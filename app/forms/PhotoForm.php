<?php

namespace app\forms;

use app\model\PhotoModel;
use Nette\Application\UI\Form;

class PhotoForm
{
    /**
     * @var \app\model\PhotoModel
     */
    private $model;

    /**
     * Konstruktor slouží k předání závislostí potřebných pro fungování formuláře
     * např databázi, model, ...
     *
     * @param \app\model\PhotoModel $model
     */
    public function __construct (PhotoModel $model)
    {
        $this->model = $model;
    }

    /**
     * Metoda create - stejný název se použije i v Presenteru v createComponent
     * vrací komponentu (formulář, ...)
     *
     * @return \Nette\Application\UI\Form
     */
    public function create ()
    {
        $form = new Form;

        $form->addText('name', 'Název fotky');

        $form->addSubmit('send');

        /**
         * Při úspěšném odeslání uložit data
         */
        $model = $this->model;
        /**
         * @param PhotoForm $form
         * @param array $values
         */
        $form->onSuccess[] = function ($form, array $values) use ($model) {

            /**
             * Zavoláme metodu addPhoto na modelu PhotoModel
             */
            $model->addPhoto($values);
        };

        /**
         * vrátit komponentu (formulář)
         */
        return $form;
    }
}
