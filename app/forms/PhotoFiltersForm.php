<?php

namespace app\forms;

use Nette\Application\UI\Form;

class PhotoFiltersForm
{
    /**
     * @return \Nette\Application\UI\Form
     */
    public function create ()
    {
        $form = new Form;

        $form->addText('name', 'Jméno');

        $form->addSubmit('send', 'Filtrovat');

        return $form;
    }
}
