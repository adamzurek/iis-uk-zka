<?php

namespace app\model;

/**
 * Repozitář tabulky photo
 */
class PhotoModel extends BaseModel
{
    /**
     * databáze se připojí v konstruktoru třídy BaseModel
     */


    /**
     * Přidá fotku podle dat $data
     *
     * @param array $data
     *
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function addPhoto (array $data)
    {
        /** do tabulky "photo" vlož data z proměnné */
        return $this->db->table('photo')
            ->insert($data);
    }

    /**
     * Vrátí konkrétní fotku, fetchnutou - potom můžu použít $photo->name, $photo->id_photo
     *
     * @param int $id
     *
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getPhoto ($id)
    {
        /**
         * z tabulky "photo" vyber data, kde primární klíč má hodnotu $id a fetchni tento řádek
         */
        return $this->db->table('photo')->wherePrimary($id)->fetch();
    }

    /**
     * Vrátí celou tabulku, bez filtrace, řazení
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getPhotos ()
    {
        /**
         * vše z tabulky "photo"
         */
        return $this->db->table('photo');
    }

    /**
     * @param array $filters
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getPhotosByFilters (array $filters)
    {
        $photos = $this->db->table('photo');

        if (!empty($filters['name'])) {
            $photos->where('name', $filters['name']);
        }

        return $photos;
    }
}
