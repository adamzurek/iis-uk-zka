<?php

namespace app\model;

/**
 * Základní model, kde si jen uložím instanci databázové třídy do proměnné/property
 */

abstract class BaseModel
{
    /**
     * Pokud uvedeme protected, můžeme tuto proměnnou používat v potomcích (např. v PhotoModel, který tuto třídu rozšiřuje - extends BaseModel)
     *
     * @var \Nette\Database\Context
     */
    protected $db;

    /**
     * Předáme si databázi do modelu
     *
     * @param \Nette\Database\Context $db
     */
    public function __construct (\Nette\Database\Context $db)
    {
        $this->db = $db;
    }
}
