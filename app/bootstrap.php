<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

/* tady můžeme nastavit IP adresu, pole IP adres, true/false a nastavit si tak režim vývoje */
$configurator->setDebugMode('123.123.123.123'); // enable for your remote IP

$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
