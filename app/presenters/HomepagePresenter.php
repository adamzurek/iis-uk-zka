<?php

namespace App\Presenters;

use app\forms\PhotoFiltersForm;
use app\forms\PhotoForm;
use app\model\PhotoModel;
use Nette;

/**
 * Presenter Homepage
 */
class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /**
     * Takto se získávají závislosti v Presenteru
     * důležitá je anotace @inject
     * musím uvést jaký typ/třídu (můžu použít use app\forms\PhotoForm a tady uvést jen zkrácený výraz) chci, anotace @ var
     *
     * @var PhotoModel
     * @inject
     */
    public $photoModel;

    /**
     * Defaultní akce homepage
     * parametrem page určuji offset
     *
     * @param int $page
     */
    public function actionDefault ($page = 1)
    {
        /**
         * vyber všechny fotky, aplikuj limit 20, offset podle stránky
         */
        $this->template->photos = $this->photoModel->getPhotos()->limit(20, 20 * ($page - 1));
    }

    /**
     * Zobrazit konkrétní fotku, podle ID
     *
     * @param int $id
     */
    public function actionShowPhoto ($id)
    {
        $this->template->photo = $this->photoModel->getPhoto($id);
    }

    /**
     * Toto tu nemusí být, hlavně musí být vytvořená šablona showForm.latte - pozor na malé písmeno na začátku
     */
    public function actionShowForm ()
    {

    }

    /**
     * Popis viz actionDefault
     *
     * @param int $page
     */
    public function actionVypisFotek ($page = 1)
    {
        /**
         * vyber všechny fotky podle filtrů, aplikuj limit 20, offset podle stránky
         */
        /** @var \Nette\Application\UI\Form $filtersForm */
        $filtersForm = $this['photoFiltersForm'];
        $filters = $filtersForm->isSuccess() ? $filtersForm->getValues(true) : array ();
        $this->template->photos = $this->photoModel->getPhotosByFilters($filters)->limit(20, 20 * ($page - 1));
    }

    /**
     * Získat instanci PhotoForm z dependency injection containeru
     *
     * @var PhotoForm
     * @inject
     */
    public $mujForm;

    /**
     * Takto se vytváří komponenta s názvem mujForm - pozor na malé písmeno na začátku
     *
     * @return \Nette\Application\UI\Form
     */
    public function createComponentMujForm ()
    {
        /**
         * Vytvořit nový formulář - volá se metoda PhotoForm->create()
         */
        return $this->mujForm->create();
    }

    /**
     * Nějaká další komponenta s názvem mujDruhejForm - opět malé písmeno na začátku
     *
     * @return \Nette\Application\UI\Form
     */
    public function createComponentMujDruhejForm ()
    {
        return $this->mujForm->create();
    }

    /**
     * @var PhotoFiltersForm
     * @inject
     */
    public $photoFiltersForm;

    /**
     * @return PhotoFiltersForm
     */
    protected function createComponentPhotoFiltersForm ()
    {
        return $this->photoFiltersForm->create();
    }
}
