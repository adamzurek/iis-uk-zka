1. udělat si subdoménu na localhostu a přesměrovat ji na 127.0.0.1 (C:\Windows\System32\Drivers\etc\hosts)
2. zaregistrovat virtuálního hosta (xampp\apache\conf\extra\httpd-vhosts.conf)
<VirtualHost iis-ukazka.localhost:80>
    ServerAdmin xzurek13@stud.fit.vutbr.cz
    DocumentRoot "D:\weby\iis ukazka\web-project\www"
    ServerName iis-ukazka.localhost
    ErrorLog "logs/iis-ukazka-error.log"
    CustomLog "logs/iis-ukazka-access.log" common
</VirtualHost>
3. V nadřazené složce (D:\weby\) vytvořit projekt composer create-project nette/web-project iis-ukazka
4. Vytvořit databázi, tabulky atd. třeba přes adminer (potom nahrajte do složky www/adminer.php)
5. nastavit údaje k databázi (app/config/config.local.neon)
6. http://iis-ukazka.localhost/ a mělo by se ukázat Congratulations atd.

SQL ukázkové db
```
#!sql
CREATE TABLE `photo` (
  `id_photo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id_photo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `photo` (`id_photo`, `name`) VALUES
(1,	'test1'),
(2,	'pes'),
(3,	'pes');
```

Jak udělat formulář jako komponentu
https://pla.nette.org/cs/best-practise-formulare-jako-komponenty

Nette dokumentace
https://doc.nette.org/cs/2.3/